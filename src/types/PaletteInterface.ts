import { DimensionInterface } from "./DimensionInterface";


enum CategorizationLabel {
  "Légère",
  "Demi-lourde",
  "Lourde",
}

type PaletteCategorization = {
  label: CategorizationLabel;
  Résistance: string;
  Indication: string;
};

export interface PaletteInterface {
  label: string;
  dimension: DimensionInterface;
  categorization: PaletteCategorization;
}
