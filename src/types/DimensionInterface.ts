export interface DimensionInterface {
  height: number;
  width: number;
  length: number;
  weight: number;
}
